#include "BSP.h"
#include "mg_api.h"


extern void UsrProcCallback(void);
unsigned char *ble_mac_addr;

int main( void )
{
    unsigned char * const ft_val = (unsigned char *)0x18000040;
    unsigned char ft_value[2] = {0xc0, 0x12};
    
    BSP_Init();
    
    SetBleIntRunningMode();
    radio_initBle(TXPWR_0DBM, &ble_mac_addr);
    
    if((*ft_val > 11) && (*ft_val < 25)){
        ft_value[1] = *ft_val;
        mg_activate(0x53);
        mg_writeBuf(0x4, ft_value, 2);
        mg_activate(0x56);
    }
    
    ble_run_interrupt_start(160*2); //320*0.625=200 ms
    
    while(1)
    {
        IrqMcuGotoSleepAndWakeup();
        
        if (0 == GetAdvEnable())
        {
            UsrProcCallback();
        }
    }
}

/**
  ******************************************************************************
  * @file    cx32l003_it.c
	* @author  MCU Software Team
	* @Version V1.0.0
  * @Date    21-Oct-2019
  * @brief   Interrupt Service Routines.
  ******************************************************************************

  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "BSP.h"
#include "cx32l003_it.h"

#define APP_Offset                    0x1010
#define BOOT_PROGRAM_DATA_START			((uint32_t)0x00000000) 		  
#define APP_ENTRY_ADDRESS					  (BOOT_PROGRAM_DATA_START+APP_Offset)	//app �����ʼ��ַ



/******************************************************************************/
/*           Cortex-M0+ Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+8)); AppEntry();}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+12)); AppEntry();}


/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+44)); AppEntry();}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+56)); AppEntry();}


/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{volatile void (*AppEntry)(void);AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+60));AppEntry();}


/******************************************************************************/
/* CX32L003 Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_xm32f1xx.s).                    */
/******************************************************************************/

/**
  * @brief This function handles GPIOA Interrupt line[3:1] interrupts.
  */
void GPIOA_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+64)); AppEntry();}

/**
  * @brief This function handles GPIOB Interrupt line[5:4] interrupts.
  */
void GPIOB_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+68)); AppEntry();}

/**
  * @brief This function handles GPIOC Interrupt line[7:3] interrupts.
  */
void GPIOC_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+72)); AppEntry();}

/**
  * @brief This function handles GPIOD Interrupt line[6:1] interrupts.
  */
void GPIOD_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+76)); AppEntry();}

/**
  * @brief This function handles FLASH Interrupt .
  */
void FLASH_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+80)); AppEntry();}

/**
  * @brief This function handles UART0 Interrupt .
  */
void UART0_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+88)); AppEntry();}

/**
  * @brief This function handles UART1 Interrupt .
  */
void UART1_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+92)); AppEntry();}

/**
  * @brief This function handles LPUART Interrupt .
  */
void LPUART_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+96)); AppEntry();}

/**
  * @brief This function handles SPI Interrupt .
  */
void SPI_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+104)); AppEntry();}

/**
  * @brief This function handles I2C Interrupt .
  */
void I2C_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+112)); AppEntry();}

/**
  * @brief This function handles TIM10 Interrupt .
  */
void TIM10_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+120)); AppEntry();}

/**
  * @brief This function handles TIM11 Interrupt .
  */
void TIM11_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+124)); AppEntry();}

/**
  * @brief This function handles LPTIM Interrupt .
  */
void LPTIM_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+128)); AppEntry();}

/**
  * @brief This function handles TIM1 Interrupt .
  */
void TIM1_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+136)); AppEntry();}

/**
  * @brief This function handles TIM2 Interrupt .
  */
void TIM2_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+140)); AppEntry();}

/**
  * @brief This function handles PCA Interrupt .
  */
void PCA_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+148)); AppEntry();}

/**
  * @brief This function handles WWDG Interrupt .
  */
void WWDG_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+152)); AppEntry();}

/**
  * @brief This function handles IWDG Interrupt .
  */
void IWDG_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+156)); AppEntry();}

/**
  * @brief This function handles ADC Interrupt .
  */
void ADC_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+160)); AppEntry();}

/**
  * @brief This function handles LVD Interrupt .
  */
void LVD_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+164)); AppEntry();}

/**
  * @brief This function handles VC Interrupt .
  */
void VC_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+168)); AppEntry();}

/**
  * @brief This function handles AWK Interrupt .
  */
void AWK_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+176)); AppEntry();}

/**
  * @brief This function handles OWIRE Interrupt .
  */
void OWIRE_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+180)); AppEntry();}

/**
  * @brief This function handles RTC Interrupt .
  */
void RTC_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+184)); AppEntry();}

/**
  * @brief This function handles CLKTRIM Interrupt .
  */
void CLKTRIM_IRQHandler(void)
{volatile void (*AppEntry)(void); AppEntry = (volatile void(*)())(*(uint32_t*)(APP_ENTRY_ADDRESS+188)); AppEntry();}

